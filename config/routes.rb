Rails.application.routes.draw do
	post :urls, to: 'links#urls', as: :urls
	get 'urls/:short_url', to: 'links#show'
	get 'urls/:short_url/stats', to: 'links#stats'
end
