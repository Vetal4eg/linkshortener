class LinksController < ApplicationController
  before_action :check_parms, only: :urls

  def show
    link = Link.find_by(slug: params[:short_url])
    link.update_count
    
    render json: link, each_serializer: LinkSerializer
  end

  def stats
    link = Link.find_by(slug: params[:short_url])
    render json: link.clicks
  end

  def urls
    service = LinkShortenerService.new(url: params[:url])
    render json: service.call
  end

  def check_parms
    return if params[:url].present?

    render_error message: 'url not set'
  end
end

