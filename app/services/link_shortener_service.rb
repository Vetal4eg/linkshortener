class LinkShortenerService
  attr_reader :url

  def initialize(url:)
    @url = url
  end

  def call
    link = Link.find_or_initialize_by url: url
    link.update(slug: SecureRandom.alphanumeric(8).downcase) if link.new_record?
    link.save!

    link.slug
  end
end