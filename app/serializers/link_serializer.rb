class LinkSerializer < ActiveModel::Serializer
  attributes :url, :clicks
end
