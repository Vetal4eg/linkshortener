class Link < ApplicationRecord
  validates :url, uniqueness: true, presence: true, format: URI::regexp(%w[http https])
  validates_uniqueness_of :slug

  def update_count
  	update(clicks: self.clicks + 1)
  end
end
