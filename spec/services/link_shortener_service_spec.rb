require 'rails_helper'

RSpec.describe LinkShortenerService do
  let(:service) { described_class.new(url: 'http://google.com/') }

  describe '#call' do
    it 'create new link' do
      expect { service.call }.to change(Link, :count).from(0).to(1)
    end
  end
end